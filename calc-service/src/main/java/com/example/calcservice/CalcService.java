package com.example.calcservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalcService
{
    @GetMapping("/add/{num1}/{num2}")
    public Response add(@PathVariable float num1, @PathVariable float num2)
    {
        return new Response(num1, num2, num1+num2);
    }
    @GetMapping("/sub/{num1}/{num2}")
    public Response sub(@PathVariable float num1, @PathVariable float num2)
    {
        return new Response(num1, num2, num1-num2);
    }
    @GetMapping("/mul/{num1}/{num2}")
    public Response mul(@PathVariable float num1, @PathVariable float num2)
    {
        return new Response(num1, num2, num1*num2);
    }
    @GetMapping("/div/{num1}/{num2}")
    public Response div(@PathVariable float num1, @PathVariable float num2)
    {
        return new Response(num1, num2, num1/num2);
    }



}
