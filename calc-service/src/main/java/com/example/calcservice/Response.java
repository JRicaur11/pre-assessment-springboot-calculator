package com.example.calcservice;

public class Response
{
    private float num1;
    private float num2;
    private float result;

    public Response(float num1, float num2, float result)
    {
        this.num1 = num1;
        this.num2 = num2;
        this.result = result;
    }

    public float getNum1()
    {
        return num1;
    }

    public void setNum1(float num1)
    {
        this.num1 = num1;
    }

    public float getNum2()
    {
        return num2;
    }

    public void setNum2(float num2)
    {
        this.num2 = num2;
    }

    public float getResult()
    {
        return result;
    }

    public void setResult(float result)
    {
        this.result = result;
    }
}
